#!/bin/sh

cat <<EOF

MRCODYSMITH -- MRCODYSMITH -- MRCODYSMITH -- MRCODYSMITH

"Goin' to Work" 

MRCODYSMITH -- MRCODYSMITH -- MRCODYSMITH -- MRCODYSMITH
EOF

echo "Let's turn this server into a MrCodySmith Server...."

# Ensure we're up to date and ready to go...
apt update
#apt ugprade -y

# install the packages we need one at a time...
apt install vim -y
apt install zsh -y 
apt install tmux -y
apt install mosh -y

# setup zsh... 
echo "Setting up your ZSH config..."
cat << EOF > ~/.zshrc
# History
setopt SHARE_HISTORY      # Instant export of commands to history file+history
setopt APPEND_HISTORY     # Allow multiple zsh sessions to coexist.
setopt EXTENDED_HISTORY   # Puts timestamps in the history.
setopt HIST_IGNORE_DUPS   # Do not allow contiguous duplicates in history.

# Completion
setopt CORRECT            # Try to correct bad commands. e.g. cta -> cat
setopt CORRECT_ALL        # Try to correct bad arguments. e.g. --hepl -> --help
setopt REC_EXACT          # Allow exact matches without beeping at me.
setopt AUTO_LIST          # Automatically list choices for ambiguous completes.

# Directories
setopt PUSHD_TO_HOME      # pushd with no args assumes home directory.
setopt PUSHD_SILENT       # pushd does not tell me things I already know.
setopt AUTO_PUSHD         # cd is considered a call to pushd.
setopt AUTO_PARAM_SLASH   # Do not have an extra character to type.
setopt EXTENDED_GLOB      # nice features like recursive globbing, negation.

# Jobs
setopt NOTIFY             # Report BG jobs immediately, not at next prompt.
setopt LONG_LIST_JOBS     # List jobs in long format by default.
setopt AUTO_RESUME        # Allow a repitition of a bg command to resume it.

# Do not allow: I hate these settings.
unsetopt BG_NICE          # Background commands run at same priority as FG
unsetopt MENUCOMPLETE     # When there is completions, let me see them first.
unsetopt GLOB_DOTS        # Require a dot to match dotfiles.

# Misc
setopt MAIL_WARNING       # You have got mail.
setopt ALL_EXPORT         # Global export next sections variables.

TZ="America/New_York"
HISTFILE=$HOME/.zhistory
HISTSIZE=5000
SAVEHIST=5000
PAGER='less'
EDITOR='vim'
MUTT_EDITOR='vim'
LC_ALL='en_US.UTF-8'
LANG='en_US.UTF-8'
LC_CTYPE=C

################################################################################
# Colors ( Force Jellybeans )
#
#

echo -ne '\e]4;0;#121212\a'   # black
echo -ne '\e]4;1;#d75f5f\a'   # red
echo -ne '\e]4;2;#87af5f\a'   # green
echo -ne '\e]4;3;#ffd787\a'   # yellow
echo -ne '\e]4;4;#87afd7\a'   # blue
echo -ne '\e]4;5;#d7afff\a'   # magenta
echo -ne '\e]4;6;#5fd7ff\a'   # cyan
echo -ne '\e]4;7;#d7d7d7\a'   # white (light grey really)
echo -ne '\e]4;8;#686a66\a'   # bold black (i.e. dark grey)
echo -ne '\e]4;9;#f54235\a'   # bold red
echo -ne '\e]4;10;#99e343\a'  # bold green
echo -ne '\e]4;11;#fdeb61\a'  # bold yellow
echo -ne '\e]4;12;#84b0d8\a'  # bold blue
echo -ne '\e]4;13;#bc94b7\a'  # bold magenta
echo -ne '\e]4;14;#37e6e8\a'  # bold cyan
echo -ne '\e]4;15;#f1f1f0\a'  # bold white

################################################################################
# Aliases
#
#

alias s='ssh'
alias dog='~/.settings/scripts/dog.py'
alias quit='exit'
alias whereami='whoami && pwd'
EOF

# setup tmux...
echo "Setting up your TMUX config..."
cat << EOF > ~/.tmux.conf
#MrCodySmith Tmux Config
set-option -g default-terminal screen
unbind C-b
set -g prefix C-a
bind C-a send-prefix
set -g terminal-overrides 'xterm*:smcup@:rmcup@'
if-shell "[[ `tmux -V | cut -d' ' -f2` -ge 2.1 ]]" 'set -g mouse on'
EOF

# setup VIM...
echo "Setting up your VIM config..."
cat << EOF > ~/.vimrc
" Crucial
set nocompatible

" Basic settings.
syntax enable
set t_Co=256
color jellybeans
set number
set backspace=indent,eol,start
set autoindent
set copyindent
set showmatch
set smartcase
set hlsearch
set incsearch
set history=500
set undolevels=500

" Handle indentation. 4 spaces behave like a tab, but aren't.
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab

" Macbook Pro specific- the esc 'button' is terrible.
inoremap jj <Esc>

" Move cursor by display lines, not real lines.
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk
nnoremap <Down> gj
nnoremap <Up> gk
vnoremap <Down> gj
vnoremap <Up> gk
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk
noremap <silent> <leader>obk :call OpenCurrentFileBackupHistory()<cr>
map <silent> <C-E> :Vex<CR>

" Easy window/pane navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-Down> <C-w>j
map <C-Up> <C-w>k
map <C-Left> <C-w>h
map <C-Right> <C-w>l

" NERDtree like netrw
let g:netrw_banner = 0
let g:netrw_dirhistmax = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_winsize = 25
augroup ProjectDrawer
  autocmd!
  autocmd VimEnter * if !argc() | :Vexplore  | endif
augroup END

" w!! will try to sudo write.
cmap w!! w !sudo tee % >/dev/null

" Auto shebang and encoding embeds depending on filetype.
augroup Shebang
  autocmd BufNewFile *.py 0put =\"#!/usr/bin/env python3\<nl># -*- coding: utf-8 -*-\<nl>\"|$
  autocmd BufNewFile *.rb 0put =\"#!/usr/bin/env ruby\<nl># -*- coding: None -*-\<nl>\"|$
  autocmd BufNewFile *.tex 0put =\"%&plain\<nl>\"|$
  autocmd BufNewFile *.\(cc\|hh\) 0put =\"//\<nl>// \".expand(\"<afile>:t\").\" -- \<nl>//\<nl>\"|2|start!
  autocmd FileType c,cpp,java,php,python autocmd BufWritePre <buffer> %s/\s\+$//e
augroup END

autocmd FileType python set cc=110
autocmd FileType c,cpp,java,php,sh set cc=80
autocmd FileType html set ft=htmljinja
EOF

# Jellybeans... 
mkdir ~/.vim/
mkdir ~/.vim/colors
cp jellybeans.vim ~/.vim/colors/jellybeans.vim

